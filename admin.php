<?php
if(isset($_COOKIE['admin'])){
	header("Location: index.php");
}
echo "
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='sui/semantic.min.css' />
<link rel='stylesheet' href='css/oldOwn.css' />
<div class='ui inverted grid center aligned' id='principalGrid'>
	<div class='nine wide column'>
		<form class='ui inverted form' action='admBack.php?function=login' method='POST'>
			<fieldset id='mopa'>
				<legend><h2>Entre na sua conta Adm.</h2></legend>
				<div class='fields'>
					<div class='six wide field'>
						<label class='ui inverted blue label' for='user'>Usuário: </label>
					</div>
					<div class='ten wide field'>
						<input type='text' name='user' id='user' required/>
					</div>
				</div>
				<div class='ui inverted divider'></div>
				<div class='fields'>
					<div class='six wide field'>
						<label class='ui inverted blue label' for='pwd'>Senha: </label>
					</div>
					<div class='ten wide field'>
						<input type='password' name='pwd' id='pwd' required />
					</div>
				</div>
				<div class='ui inverted divider'></div>
				<div class='three fields'>
					<div class='field submit'>
						<input type='submit' class='ui inverted yellow button submit' value='Entrar'/>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
";


?>