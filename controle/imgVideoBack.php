<?php

require_once('../lib/Conection.class.php');
$conection = new Conection('../lib/mysql.ini');
$arqCod = base64_encode(file_get_contents($_FILES['image']['tmp_name']));
$arqType = $_FILES['image']['type'];
$sql = "INSERT INTO Image(data, type) VALUES (:data, :type);";
$command = $conection->getConection()->prepare($sql);
$command->bindParam('data', $arqCod);
$command->bindParam('type', $arqType);
$command->execute();
$conection->__destruct();
header("Location: ../img.php");
?>