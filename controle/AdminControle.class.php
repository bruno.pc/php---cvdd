<?php
require_once('lib/Conection.class.php');
require_once('modelo/Admin.class.php');
class AdminControle{
	public function login($logPhp){
		$conection = new Conection('lib/mysql.ini');
		$sql = "SELECT user, pwd FROM Users where user='{$logPhp->getUser()}';";
		$command = $conection->getConection()->prepare($sql);
		$command->execute();
		$logBd = $command->fetch();
		$conection->__destruct();
		if(isset($logBd->user) && $logPhp->getPwd() == $logBd->pwd){
			setcookie('admin', "$logBd->user", time()+3600);
			header('Location: index.php');
		}else{
			echo "Usuário ou senha incorretos";
		}
	}
	public function getLogin($user){
		$conection = new Conection('lib/mysql.ini');
		$sql = "SELECT user, pwd FROM Users where user='{$user}';";
		$command = $conection->getConection()->prepare($sql);
		$command->execute();
		$cuLogin = $command->fetch();
		$conection->__destruct();
		return $cuLogin;
	}
	public function editLogin($logPhp){
		$conection = new Conection('lib/mysql.ini');
		$sql = "UPDATE Users SET user=:user, pwd=:pwd";
		$command = $conection->getConection()->prepare($sql);
		$command->bindParam("user", $logPhp->getUser());
		$command->bindParam("pwd", $logPhp->getPwd());
		$command->execute();
		$conection->__destruct();
		header("Location: index.php");
	}
}