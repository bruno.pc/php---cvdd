<?php
require_once("modelo/Post.class.php");
require_once("controle/PostControle.class.php");
$post = new Post();
$postControl = new PostControle();
$post->setTitle($_POST['title']);
$post->setSubtitle($_POST['subtitle']);
$post->setContent($_POST['content']);
$post->setPostType($_POST['type']);
$post->setId_post($postControl->addPost($post));
if(!empty($_FILES['img']['tmp_name'])){
	$imgCod = base64_encode(file_get_contents($_FILES['img']['tmp_name']));
	$post->setImageData($imgCod);
	$post->setImageType($_FILES['img']['type']);
}else{
	$post->setImageData('0');
	$post->setImageType('0');
}
$postControl->addImage($post);
if(!empty($_FILES['video']['type'])){
	$videoCod = base64_encode(file_get_contents($_FILES['video']['tmp_name']));
	$post->setVideoData($videoCod);
	$post->setVideoType($_FILES['video']['type']);
}else{
	$post->setVideoData('0');
	$post->setVideoType('0');
}
$postControl->addVideo($post);
header("Location: index.php");
?>