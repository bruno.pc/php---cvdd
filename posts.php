<?php
require_once("controle/PostControle.class.php");
$post= new PostControle();
$postzin = $post->onlyAPost($_GET['id']);
echo "
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='sui/semantic.min.css'>
<link rel='stylesheet' href='css/own.css'>
<div class='ui labeled icon vertical inverted sidebar menu'>
	<div class='ui header item'>
		<img class='ui massive image' id='logoSide' src='LogoCVDD.png'>
	</div>
	<a href='index.php' class='active item'>
		<i class='home icon'></i>
		Início
	</a>
	<a class='header item' href='content.php?type=tuto'>
		<i class='newspaper outline icon'></i>
		Ultimas Postagens Sobre:
	</a>
	<a class='item' href='content.php?type=cursos'>
		<i class='book icon'></i>
		Cursos
	</a>
	<a class='item' href='content.php?type=projetos'>
		<i class='clipboard outline icon'></i>
		Projetos
	</a>
	<a class='item' href='content.php?type=eventos'>
		<i class='map marker icon'></i>
		Eventos
	</a>
	"; 
//Conteudo admin
if(isset($_COOKIE['admin'])){
echo "
	<div class='borderless item'></div>
	<div class='item'></div>
	<div class='header item'>Menu Admin</div>
	<a href='addPost.php' class='item'>
		<i class='plus icon'></i>
		Adicionar Posts
	</a>
	<a href='editLogin.php' class='item'>
		<i class='user outline icon'></i>
		Configurações de Conta
	</a>
	<a href='logout.php' class='item'>
		<i class='close icon icon'></i>
		Logout
	</a>
";
}
//Fim do menu
echo "
</div>
<div class='pusher'>
	<div class='ui inverted two item fixed menu'>
		<center>
			<div class='item borderless'>
				<a id='sidebar' class='ui inverted teal button'>Menu</a>
			</div>
		</center>
	</div>
	<div class='ui inverted two item menu'>
		<center>
			<div class='item borderless'>
			</div>
		</center>
	</div>
	<div id='oAP'>
		<div class='fundo3'>
			<h1 style='text-align:center; font-size:40px'>{$postzin->getTitle()}</h1>
		";
		if($postzin->getImageType()!='0'){
	echo 	"<center><img src='data:{$postzin->getImageType()};base64, {$postzin->getImageData()}'></center>";
		}
echo "
			<div class='ui divider'></div>
			<h2 style='text-align:center; font-size:20px'>{$postzin->getSubtitle()}</h2>
			<div class='ui divider'></div>
			<br><br>
			<div id='myContent'>
				{$postzin->getContent()}
			</div>
			<div class='ui divider'></div>
			<br>";
			if($postzin->getVideoType()!='0'){

				echo "<center><video style='width:70%' src='data:{$postzin->getVideoType()};base64, {$postzin->getVideoData()}' controls></video></center>";
			}
echo "
		</div>
		<br><br>
	</div>
	
	<br>
	<div class='ui divider footerDivider'></div>
	<div class='ui inverted segment' id='footer'>
		<div class='ui inverted six columns stackable grid'>
			<div class='column'></div>
			<div class='column'></div>
			<div class='column'>
				Rua Jerônimo de Albuquerque, 445 <br>
				Barra do Ceará, Fortaleza-CE <br>
				Próximo a AMBEV <br>
				<i class='whatsapp icon'></i>86 8769-7341
			</div>
			<div class='column'>
				Criado por: Bruno Paiva
				Disponibilização dos Conteúdos: <br>
				Casa de Vovó Dedé<br>
				Fábrica de Software/Facebook
			</div>
			<div class='column'></div>
			<div class='column'></div>
			</center>
		</div>
	</div>
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script>
$('#sidebar').click(function(){
	$('.ui.sidebar').sidebar('toggle');
});
</script>
";
?>