<?php
if(!isset($_COOKIE['admin'])){
	header("Location: index.php");
}
require_once('controle/AdminControle.class.php');
$adm = new AdminControle();
$cuLogin = $adm->getLogin($_COOKIE['admin']);
echo "
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='sui/semantic.min.css' />
<link rel='stylesheet' href='css/oldOwn.css' />
<div class='ui labeled icon vertical inverted sidebar menu'>
	<a href='index.php' class='item'>
		<i class='home icon'></i>
		Início
	</a>
	<a class='header item' href='content.php?type=tuto'>
		<i class='newspaper outline icon'></i>
		Ultimas Postagens Sobre:
	</a>
	<a class='item' href='content.php?type=cursos'>
		<i class='book icon'></i>
		Cursos
	</a>
	<a class='item' href='content.php?type=projetos'>
		<i class='clipboard outline icon'></i>
		Projetos
	</a>
	<a class='item' href='content.php?type=eventos'>
		<i class='map marker icon'></i>
		Eventos
	</a>
	"; 
//Conteudo admin
if(isset($_COOKIE['admin'])){
echo "
	<div class='borderless item'></div>
	<div class='item'></div>
	<div class='header item'>Menu Admin</div>
	<a href='content.php' class='item'>
		<i class='plus icon'></i>
		Adicionar Posts
	</a>
	<a href='editLogin.php' class='active item'>
		<i class='user outline icon'></i>
		Configurações de Conta
	</a>
	<a href='logout.php' class='item'>
		<i class='close icon icon'></i>
		Logout
	</a>
";
}
//Fim do menu
echo "
</div>
<div class='pusher'>
	<div class='ui inverted two item fixed menu'>
		<center>
			<div class='item borderless'>
				<a id='sidebar' class='ui inverted teal button'>Menu</a>
			</div>
		</center>
	</div>
	<div class='ui inverted grid center aligned' id='principalGrid'>
		<div class='nine wide column'>
			<form class='ui inverted form' action='admBack.php?function=edit' method='POST'>
				<fieldset id='mopa'>
					<legend><h2>Mudança de Login</h2></legend>
					<div class='fields'>
						<div class='six wide field'>
							<label class='ui inverted blue label' for='user'>Usuário: </label>
						</div>
						<div class='ten wide field'>
							<input type='text' value='{$cuLogin->user}' name='user' id='user'/>
						</div>
					</div>
					<div class='ui inverted divider'></div>
					<div class='fields'>
						<div class='six wide field'>
							<label class='ui inverted blue label' for='pwd'>Senha: </label>
						</div>
						<div class='ten wide field'>
							<input type='password' name='pwd' id='pwd' />
						</div>
					</div>
					<div class='ui inverted divider'></div>
					<div class='three fields'>
						<div class='field submit'>
							<input type='submit' class='ui inverted yellow button submit' value='Mudar o Login'/>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script>
$('#sidebar').click(function(){
	$('.ui.sidebar').sidebar('toggle');
});
</script>
";
?>