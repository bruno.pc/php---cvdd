<?php
require_once("controle/PostControle.class.php");
$post = new PostControle();
echo "
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='sui/semantic.min.css'>
<link rel='stylesheet' href='css/own.css'>
<div class='ui labeled icon vertical inverted thin sidebar menu'>
	<div class='ui header item'>
		<img class='ui massive image' id='logoSide' src='LogoCVDD.png'>
	</div>
	<a href='index.php' class='item'>
		<i class='home icon'></i>
		Início
	</a>
	<a class='header active item' href='content.php?type=tuto'>
		<i class='newspaper outline icon'></i>
		Ultimas Postagens Sobre:
	</a>
	<a class='item' href='content.php?type=cursos'>
		<i class='book icon'></i>
		Cursos
	</a>
	<a class='item' href='content.php?type=projetos'>
		<i class='clipboard outline icon'></i>
		Projetos
	</a>
	<a class='item' href='content.php?type=eventos'>
		<i class='map marker icon'></i>
		Eventos
	</a>
	"; 
//Conteudo admin
if(isset($_COOKIE['admin'])){
echo "
	<div class='borderless item'></div>
	<div class='item'></div>
	<div class='header item'>Menu Admin</div>
	<a href='addPost.php' class='item'>
		<i class='plus icon'></i>
		Adicionar Posts
	</a>
	<a href='editLogin.php' class='item'>
		<i class='user outline icon'></i>
		Configurações de Conta
	</a>
	<a href='logout.php' class='item'>
		<i class='close icon icon'></i>
		Logout
	</a>
";
}
//Fim do menu
echo "
</div>
<div class='pusher'>
	<div class='ui inverted two item fixed menu'>
		<center>
			<div class='item borderless'>
				<a id='sidebar' class='ui inverted teal button'>Menu</a>
			</div>
		</center>
	</div>
	<div class='ui inverted two item menu'>
		<center>
			<div class='item borderless'>
			</div>
		</center>
	</div>

	<center><h1 class='ui inverted header'>Postagens Recentes</h1><br>
	<div class='ui inverted grey four item fluid stackable menu'>
		<a class='item' href='content.php?type=tuto'>Todas Publicações</a>
		<a class='item' href='content.php?type=cursos'>Cursos</a>
		<a class='item' href='content.php?type=eventos'>Eventos</a>
		<a class='item' href='content.php?type=projetos'>Projetos</a>
	</div>
	<br>
	</center>
	<div class='ui inverted four columns stackable grid' id='contentGrid'>";
	$tuto;
	if(!isset($_GET['type']) || $_GET['type'] == 'tuto'){
		$tuto = $post->allPosts();
	}else if($_GET['type'] == 'cursos'){
		$tuto = $post->allPostType("cursos");
	}else if($_GET['type'] == 'projetos'){
		$tuto = $post->allPostType("projetos");
	}else if($_GET['type'] == 'eventos'){
		$tuto = $post->allPostType("eventos");
	}
if(!empty($tuto)){
	foreach($tuto as $postzin){
		if($postzin->getImageData() == '0'){
		echo "
		<div class='column'>
			<div class='noImgPost'>
				<h1 class='ui header'><a href='posts.php?id={$postzin->getId_Post()}'>{$postzin->getTitle()}</a></h1>
				<div class='ui divider postPagesDivider'></div>
				<div class='postPagesText'>
					<h3>{$postzin->getSubtitle()}</h3>
					<a href='posts.php?id={$postzin->getId_Post()}' style='font-weight:bold'>Continuar lendo...</a>
				</div>";
				if(isset($_COOKIE['admin'])){
		echo "
				<br>
				<div class='ui divider postPagesDivider'></div>
				<a class='ui inverted blue tiny button' href='editPost.php?id={$postzin->getId_Post()}'>Editar</a>
				<a class='ui inverted red tiny button ativarModal'  href='confirmDelete.php?id={$postzin->getId_Post()}'>Excluir</a>";
				}
echo "		</div>	
		</div>";
		}else{
		echo "
		<div class='column'>
			<div class='ui card'>
				<div class='image'>
					<img src='data:image/png;base64, {$postzin->getImageData()}' >
				</div>
				<div class='content cardCont'>
					<h3 class='ui header'><a href='posts.php?id={$postzin->getId_Post()}'> {$postzin->getTitle()}</a></h3>
				</div>";
			if(isset($_COOKIE['admin'])){
			echo "
				<br>
				<div class='content'>
					<center>
						<a class='ui inverted blue tiny button' href='editPost.php?id={$postzin->getId_Post()}'>Editar</a>
						<a class='ui inverted red tiny button ativarModal' href='confirmDelete.php?id={$postzin->getId_Post()}'>Excluir</a>
					</center>
				</div>";
			}
echo "		</div>
		</div>";
		}
	}
}else{
	echo "<div class='centered row'><div class='column'><h1 class='ps'>Sem publicações.</h1></div></div>";
}
echo "	
	</div>";
	echo "
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script>
$('#sidebar').click(function(){
	$('.ui.sidebar').sidebar('toggle');
});
</script>
";
?>