<?php
require_once("modelo/Post.class.php");
require_once("controle/PostControle.class.php");
$post = new Post();
$postControl = new PostControle();
$videoImg = $postControl->onlyAPost($_GET['id']);
$post->setId_Post($_GET['id']);
$post->setTitle($_POST['title']);
$post->setSubtitle($_POST['subtitle']);
$post->setContent($_POST['content']);
$post->setPostType($_POST['type']);
$postControl->updatePost($post);
if(!empty($_FILES['img']['tmp_name'])){
	$imgCod = base64_encode(file_get_contents($_FILES['img']['tmp_name']));
	$post->setImageData($imgCod);
	$post->setImageType($_FILES['img']['type']);
}else{
	$post->setImageData($videoImg->getImageData());
	$post->setImageType($videoImg->getImageType());
}
$postControl->updateImage($post);
if(!empty($_FILES['video']['type'])){
	$videoCod = base64_encode(file_get_contents($_FILES['video']['tmp_name']));
	$post->setVideoData($videoCod);
	$post->setVideoType($_FILES['video']['type']);
}else{
	$post->setVideoData($videoImg->getVideoData());
	$post->setVideoType($videoImg->getVideoType());
}
$postControl->updateVideo($post);
header("Location: index.php");
?>