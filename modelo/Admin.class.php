<?php
class Admin{
	private $user;
	private $pwd;
	public function getUser(){
		return $this->user;
	}
	public function setUser($u){
		$this->user = (isset($u)) ? $u : NULL;
	}
	public function getPwd(){
		return $this->pwd;
	}
	public function setPwd($p){
		$this->pwd = (isset($p)) ? $p : NULL;
	}
}
?>